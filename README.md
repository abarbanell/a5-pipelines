# a5-pipelines - Docker container

This is the docker container used for testing Angular 5 applications with bitbucket pipelines. 

It is in the [docker registry](https://cloud.docker.com/swarm/abarbanell/repository/registry-1.docker.io/abarbanell/a5-pipelines/general)
 as abarbanell/a5-pipelines

## How to build

You need to push changes of this repo to its bitbucket master at 
[abarbanell/a5-pipelines](https://bitbucket.org/abarbanell/a5-pipelines])

Before that you should set the correct variables for DOCKER_USER and DOCKER_PASSWORD in the bitbucket pipelines [environment settings](https://bitbucket.org/abarbanell/a5-pipelines/admin/addon/admin/pipelines/repository-variables).

Make sure to set the DOCKER_PASSWORD as *private*!



## scope

 It should replicate the following setup from bitbucket_pipelines.yml.

 Old:

```
image: node:latest

pipelines:
  default:
    - step:
        caches: 
          - node
        script: 
          - apt-get update
          - apt-get install -y gettext-base 
          - echo 'deb http://dl.google.com/linux/chrome/deb/ stable main' > /etc/apt/sources.list.d/chrome.list
          - wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
          - set -x && apt-get update && apt-get install -y xvfb google-chrome-stable
          - wget -q -O /usr/bin/xvfb-chrome https://bitbucket.org/atlassian/docker-node-chrome-firefox/raw/ff180e2f16ea8639d4ca4a3abb0017ee23c2836c/scripts/xvfb-chrome
          - ln -sf /usr/bin/xvfb-chrome /usr/bin/google-chrome
          - chmod 755 /usr/bin/google-chrome
          - # start your actual tests here
          - npm install
          - npm test
```

New: 

```
image: abarbanell/a5-pipelines

pipelines:
  default:
    - step:
        caches: 
          - node
        script: 
          - # start your actual tests here
          - npm install
          - npm test
```
